<?php require_once('php/head.php'); ?>
<?php require_once('php/start.php'); ?>

<div class="col-md-6">

<p>Utan dessa tappra varelsers insatser hade kongressen kanske inte blivit av!</p>

<dl>
  <dt>
    Anna Kashubina
  </dt>
  <dd>
Websida - design och kod, ansvarig för zombies
  </dd>

  <dt>
    Peter Bengtsson
  </dt>
  <dd>
Diverse teknik, allmän hjälpreda, ordnar fika, skyldig till tipspromenaden
  </dd>

  <dt>
    Louise Rylander
  </dt>
  <dd>
Kongressgeneral, ansvarig för kongressens framtida historia
  </dd>

  <dt>
    Helena Kiel
  </dt>
  <dd>
Ekonomiskt ansvarig, hanterar berggrunden och ser till att kongressen inte drabbas av jordbävningar eller vulkanutbrott
  </dd>

  <dt>
    Lars-Göran Johansson
  </dt>
  <dd>
Biografanläggning, uppmuntran av medlemmarna, underhåll av cybernetiska livsformer
  </dd>

  <dt>
Glenn Petersen
  </dt>
  <dd>
    Bokrekommendationer, ekrekommendationer
  </dd>

  <dt>
Thomas Olsson
  </dt>
  <dd>
Baransvarig, barbaransvarig
  </dd>

  <dt>
Rudolf der Hagopian
  </dt>
  <dd>
Rösträknare, rasträknare, resträknare, testräknare
  </dd>
</dl>

</div>

<?php require_once('php/end.php'); ?>
