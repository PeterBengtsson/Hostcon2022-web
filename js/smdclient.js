'use strict';
/*
 * Very simple RPC implementation for SMD.
 *
 * Possible improvements which might or might not be implemented some day:
 *		+ Batch calls.
 *
 *	©2022 Peter Bengtsson <tesla@och.nu>, licensed under the EUPL version 1.2 or later
 */

/**
 * SMDClient() - constructor
 *
 * Currently defined options:
 * 	sync			- Use synchronous requests and return plain values. (default: true)
 * 	sync_init	- Perform a synchronous initialization in async mode to support URI as argument. (default: true)
 * 	pos_params	- Use positional parameters. If set to false, named parameters will be used when an object is passed as the only parameter to a call. (default: true)
 * 	autowrap_media	- For services with a media description (content-type and optionally encoding [only base64 supported really]), convert to a data: URI before returning the result.
 * 	debug			- Log some debug information to the console.
 *
 * @param	{String} text - The Service Mapping Description (SMD) as a string, javascript object or as an URI. Using an URI currently requires a synchronous request.
 * @param	{Object} options - Optional options
 * @return	{SMDClient/Proxy} - The RPC proxy object which represents the remote resource.
 *
 */
function SMDClient(text,options) {//{{{
	var smd=null,req,p;

	this.opts={	// Default values for options
		sync:true,
		sync_init:true,
		pos_params:true,
		autowrap_media:false,
		debug:false
	};
	this.mlist={};	//Cached method wrappers
	this.props={};	//Saved properties for the proxy
	this.smd=null;
	this.error=false;
	this.callindex=0;

	/* Override default options with user supplied values if any. */
	if(typeof(options)=='object' && options!==null) {
		for(var opt in this.opts) {
			if(typeof(options[opt])!='undefined') {
				this.opts[opt]=options[opt];
			}
		}
		if('async' in options) {	//Allow 'async:true' instead of 'sync:false' for convenience
			this.opts.sync=!options.async;
		}
	}

	/* This is a bit of a hack. If the user does not supply the SMD as text or JSON but an URI, then we do a synchronous fetch unless sync_init is also set to false. */
	if(typeof(text)=='object') {
		smd=text;
	} else {
		try {
			smd=JSON.parse(text);
		} catch(ex) {
			try {
				if(this.opts.sync===true || this.opts.sync_init===true) {
					req=new XMLHttpRequest();
					req.open("GET",text,false);
					req.send();
					if(req.readyState==4) {
						smd=JSON.parse(req.responseText);
					} else {
						throw new Error("Failed to read SMD from server");
					}
				} else {
					throw new Error("Async initialization requires the SMD as text or object. Set the 'sync_init' option to true for URI initialization with async mode.");
				}
			} catch(ex) {
				throw ex;
			}
		}
	}

	this.SetupSMD(smd);

	/* Create a proxy object to handle the actual invocations */

	p=new Proxy(this,{
		get:function(target,propname,proxy) {
			if(typeof(target.smd.services[propname])!='undefined') {
				if(!(propname in target.mlist)) {	/* Cache dispatch wrappers once created */
					if(target.opts.sync===true) {
						target.mlist[propname]=function() {return(target.dispatch_sync(propname,target.getInvocation(propname,arguments)));};
					} else {
						target.mlist[propname]=function() {return(target.dispatch_async(propname,target.getInvocation(propname,arguments)));};
					}
				}
				return(target.mlist[propname]);
			}
			return(target.props[propname]);	/* Returns undefined if undefined, so no loss if the code does not check that the property is defined and an error for unset properties is actually what we want. */
		},
		set:function(target,propname,val,proxy) {
			target.props[propname]=val;	/* Store values so the proxy looks more like a regular object */
		}
	});

	return(p);
}//}}}


/**
 * Configure the client using a parsed SMD
 *	@param {Object} smd - The parsed SMD
 */
SMDClient.prototype.SetupSMD=function(smd) {
	/* Sanity checks */
	if(smd.SMDVersion!='2.0') {
		throw new Error("Unsupported SMD version. Only version 2.0 is supported.");
	}
	if(smd.transport!='POST') {
		throw new Error("Unsupported transport. Only POST is supported.");
	}
	if(smd.envelope!='JSON-RPC-2.0') {
		throw new Error("Unsupported envelope. Only JSON-RPC-2.0 is supported.");
	}
	if(typeof(smd.services)!='object' || smd.services===null) {
		throw new Error("Service description lacks services.");
	}
	for(let x in smd.services) {
		if(this.opts.debug) {
			console.log("Configuring RPC method: "+x);
		}
		smd.services[x].has_result=typeof(x.returns)=='undefined' || (x.returns!==null && (typeof(x.returns.type)=='undefined' || (x.returns.type!==null && x.returns.type!=='null')));
		smd.services[x].name=x;
		if(typeof(smd.services[x].contentType)=='undefined') {
			smd.services[x].contentType=smd.contentType;
		}
	}
	/* Save the service description */
	this.smd=smd;
}

/**
 * Return an object suitable for JSON serialisation to call the named method with the supplied parameters.
 *
 * @param 	{String} mname		- Method name
 * @param	{Iterable} params	- Method parameters
 * @return	{Object}
 */
SMDClient.prototype.getInvocation=function(mname,params) {//{{{
	var srv,target,plist,s,pl;

	srv=this.smd.services[mname];
	pl=srv.parameters;
	target=(srv.target)?srv.target:this.smd.target;

	if(this.opts.pos_params || params.length>1 || (params.length==1 && typeof(params[0])!='object')) {	/* Positional parameters option set, more than one parameter or (first) argument is not an object */
		plist=Array.prototype.slice.call(params).map( (x,idx) => {	/* Undefined parameters are replaced with default values if they exist, otherwise throw an error. */
			if(x!==undefined) {
				return(x);
			} else if(idx<pl.length && typeof(pl[idx].default)!='undefined') {
				return(pl[idx].default);
			}
			throw new Error("Missing parameter in call to "+mname);
		});
	} else if(params.length==1) {	/* Named parameter are used iff named parameters are enabled and there is exactly one argument which is an object  */
		plist=params[0];
		for(var p of pl) {
			if(typeof(plist[p.name])=='undefined' && typeof(p.default)!='undefined') {
				plist[p.name]=p.default;
			} else if(typeof(p.optional)=='undefined' || p.optional===false) {
				throw new Error(`Missing parameter ${p.name} in call to ${mname}`);
			}
		}
	} else {
		plist=[];	/* No parameters, use an empty array for this case . */
	}

	if(typeof(srv.additionalParameters)!='undefined' && srv.additionalParameters===false && pl.length<plist.length) {
		throw new Error("Too many arguments in call to "+mname);
	}

	s={
		"jsonrpc":"2.0",
		"method":mname,
		"params":plist
	};

	/* Methods which return 'void' are assumed to be notifications and the return value is skipped by not supplying an id, but
 	 * all other invocations get an id so we can return a value.
 	 */
	if(srv.has_result) {
		s.id="c"+this.callindex;
		this.callindex++;
	}

	return(s);
}//}}}


SMDClient.prototype.autowrap=function(srv,retv) {
	if(typeof(srv.media)!='undefined') {
		if(this.opts.debug) {
			console.log(`Wrapping returnvalude from ${srv.name} in data-uri.`);
		}
		if(typeof(srv.media.binaryEncoding)!='undefined') {
			retv.result=`data:${srv.media.type};${srv.media.binaryEncoding},{$retv.result}`;
		} else {
			retv.result=`data:${srv.media.type},${retv.result}`;
		}
	}
	return(retv);
}

/**
 * Synchronous remote call which returns a plain value.
 *
 * @param {String} mname	- Method name
 * @param {Object} obj		- Call object
 * @return {any}
 */
SMDClient.prototype.dispatch_sync=function(mname,obj) {//{{{
	var srv,target,req,retv;

	srv=this.smd.services[mname];
	target=(srv.target)?srv.target:this.smd.target;

	try {
		req=new XMLHttpRequest();
		if(typeof(obj.id)!='undefined') {	/* If there is an id, then we expect a response. */
			req.open("POST",target,false);
			req.send(JSON.stringify(obj));
			if(req.readyState==4) {
				retv=JSON.parse(req.responseText);
			} else {
				throw new Error("Failed to read RPC response from server");
			}
		} else {	/* There is no response, so we just fire away the request and return true. */
			req.open("POST",target,true);
			req.send(JSON.stringify(obj));
			retv={result:true};
		}
	} catch(ex) {
		throw ex;
	}

	if(retv.error) {
		throw retv.error;
	}

	if(this.opts.autowrap_media) {
		retv=this.autowrap(srv,retv);
	}
	return(retv.result);
}//}}}

/**
 * Asynchronous remote call which returns a promise.
 *
 * @param {String} mname	- Method name
 * @param {Object} obj		- Call object
 * @return {Promise}
 */
SMDClient.prototype.dispatch_async=function(mname,obj) {//{{{
	var srv,target,retv;

	srv=this.smd.services[mname];
	target=(srv.target)?srv.target:this.smd.target;

	if(typeof(obj.id)!='undefined') {	/* If there is an id, then we expect a response. */
		retv=window.fetch(target,{method:'POST',credentials:'same-origin',body:JSON.stringify(obj)}).then(
			(resp) => {
				if(!resp.ok) {
					throw new Error("Network error. Failed to dispatch RPC call.");
				}
				return(resp.json());
			}
		).then(
			(j) => {
				if(j.error) {
					throw j.error;
				}
				if(this.opts.autowrap_media) {
					j=this.autowrap(srv,j);
				}
				return(j.result);
			}
		).catch(
			err => {throw err;}
		);
	} else {	/* There is no response, so we just fire away the request and return true. */
		window.fetch(target,{method:'POST',credentials:'same-origin',body:JSON.stringify(obj)});
		retv=Promise.resolve(true);
	}
	return(retv);
}//}}}
