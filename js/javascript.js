
function highlights () {
	const activePage = window.location;
  const navLinks = document.querySelectorAll('a.nav-link');
  navLinks.forEach(link => {
    if(link.href==activePage) {
      link.classList.add('active');
    }
  });
}

window.addEventListener("load", highlights,{once:true});

