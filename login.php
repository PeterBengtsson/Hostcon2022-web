<?php 
session_start();

if($_SERVER['REQUEST_METHOD']=='POST') {
	if(isset($_POST['user']) && isset($_POST['pass']) && $_POST['user']=='admin' && $_POST['pass']=='secret') {
		$_SESSION['HOSTCONAUTH']=1;
	}
}

if(isset($_SESSION['HOSTCONAUTH']) && $_SESSION['HOSTCONAUTH']==1) {
	header("Location: admin.php");
	exit(0);
}

require_once('php/head.php');
?>
<style type=text/css>
.loginbox {
	display: block;
	margin: auto;
	width: 32ch;
}
.loginbox * {
	padding: 0;
	margin: 0;
	border-collapse: separate;
	border: none;
}
.loginbox button {
	color: #000000;
}
.loginbox table tr {
}
.loginbox table tr td:first-child {
	text-align: right;
}
.loginbox input {
	color: #000000;
	background-color: #fafae8;
	margin-left: 4px;
	max-width: 20ch;
}
</style>
<?php require_once('php/start.php'); ?>

<form action=login.php method=POST class=loginbox>
	<table>
		<tr><td>Username<td><input name=user>
		<tr><td>Password<td><input type=password name=pass>
		<tr><td colspan=2><button>Log in</button>
	</table>
</form>

<?php require_once('php/end.php'); ?>
