<?php require_once('php/head.php'); ?>
<script type=text/javascript>
var rpc;
const q=(s) => document.querySelector(s);

async function make_memberlist() {
	const members=await rpc.GetMemberList();

	members.forEach( (info) => {
		let ol=q("#memberlist");
		let item=document.createElement("LI");
		item.textContent=info.name;
		ol.append(item);
	})
};

async function add_member(evt) {
	evt.preventDefault();
	const name=q("#newname").value;
	const email=q("#newemail").value;
	const note=q("#newnote").value;
	const visible=q("#newvisible").checked;
	try {
		await rpc.AddMember(email,name,visible,note);
		document.location.reload();
	} catch(err) {
		alert(err.message);	
	}
}

async function init() {
	const desc = await fetch("userapi.php");
	rpc=new SMDClient(await desc.text(),{async:true});

	make_memberlist();

	q("#newname").value="";
	q("#newemail").value="";
	q("#newnote").value="";
	q("#newvisible").checked=true;

  q("#btn-addnewmember").addEventListener("click",add_member,false);
}

window.addEventListener("load", init, {once:true});
</script>
<?php require_once('php/start.php'); ?>

   <div class="col-md-6">
     <span><p>Anmälan till kongressen sker genom att anmälningsformuläret på sidan fylls i – om man inte är medlem i Club Cosmos tas en smärre avgift om 100kr ut, vilken kan betalas på plats eller i förväg till Club Cosmos plusgiro: 436&nbsp;77&nbsp;95&nbsp;-&nbsp;4. Fika ingår i medlemsavgiften.</p>

     <p>Registrering av deltagande i kongressen kan ske fram till och med den 7e november 2022 CE.</p></span>

  <div id="container">
   <input type="text" placeholder="Namn" id="newname">
   <input type="email" name="email" placeholder="E-mail" id="newemail">
   <textarea rows="5" cols="45" name="comment" placeholder="Skriv en kommentar (allergier, önskningar etc.)" id="newnote"></textarea>
   <div id="remember-container">
     <input type="checkbox" class="checkbox" checked="checked" id="newvisible"/>
     <span id="remember">Visa mig i listan</span>
   </div>
   <button id="btn-addnewmember">Registera mig</button>
 </div>
</div> <!-- -->

<hr>

<div class="col-md-12">


<h3>Deltagare</h3>

<ol id="memberlist">
</ol>

</div>

</div>

<?php require_once('php/end.php'); ?>
