# TODO

## Pages
* Start/landing page
    * Formatting .
* Program page
    * [Optional] Extended information (clickable)
    * Introductory text
* Registration page
    * Information text
    * Registration form : Name, email, comment .
    * Registration button
    * Checkbox for visible/invisible registration 
    * List of registered participants except those not visible
    * End-date for registration handled
* Administration page (password protected)
    * Password to access the page
    * List all members
    * Update information about member
    * Delete memeber
* About page (with a list of organizers and contributors)

## Functionality
* An embedded map showing the location of the convention
* Registration on the registration page
* Updating and removal of registrations on the administration page

## Testing
* Chrome, Firefox and optionally Safari (Edge is no longer relevant since it uses the same rendering engine as Chrome)
* Desktop and mobile
* Portrait/landscape orientation


# TODO Peter

* Write content
* Send more cat pictures
* Functions for registration page:
    * Get a list of registered and visible memebers
    * Register a new member
* Functions for admin page:
    * Get a list of all members
    * View all details for a member
    * Update a member
    * Delete a member
