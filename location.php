<?php require_once('php/head.php'); ?>
<?php require_once('php/start.php'); ?>

   <div class="col-md-6">
     <span>
       <p>
         Club Cosmos ho:stkongresser är tillbaka i Argostugan vilken, senast vi undersökte saken, fortfarande befinner sig på <a href="http://maps.google.se/maps?f=q&source=s_q&hl=en&geocode=&q=Ekedalsgatan+26,+G%C3%B6teborg&aq=0&sll=61.606396,21.225586&sspn=32.973326,64.951172&ie=UTF8&hq=&hnear=Ekedalsgatan+26,+414+68+G%C3%B6teborg,+V%C3%A4stra+G%C3%B6talands+L%C3%A4n&ll=57.687084,11.931292&spn=0.004399,0.007929&t=h&z=17" target="_blank">Ekedalsgatan 26</a>. Stugan är inspekterad av Antizombex och var 2022-09-16 helt fri från zombievirus, marsianska virus, vampyrvirus, andromedavirus och miniatyrubåtar.
       </p>
       <p>Vet man inte redan hur man tar sig dit så är närmsta hållplats Ekedal där spårvagn 11 vanligtvis stannar. Sedan är det bara att bege sig upp för backen och hålla utkik på höger sida.</p>
     </span>
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2132.717877840976!2d11.9287889!3d57.6872726!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x464ff33a67cc3303%3A0x244da9e6f93dd536!2sEkedalsgatan%2026%2C%20414%2068%20G%C3%B6teborg!5e0!3m2!1sen!2sse!4v1663186363203!5m2!1sen!2sse" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
   </div>

<div class="img-n-h5-location">
<h5>Argostugan</h5>
   <img class="location-place"src="images/argostugan.jpg" alt="place">
 </div>

<?php require_once('php/end.php'); ?>
