<?php require_once('php/head.php'); ?>

<script type=text/javascript>
function pageInit() {
	console.log("Need more cats!");
}
</script>

<?php require_once('php/start.php'); ?>


         <div class="col-md-6">
					 <span>
					 <p>Som kongresser bör, så har även denna ett program med mer eller mindre riskfyllda aktiviteter. Rekommenderad dos aktiviteterär 3-4 per timme om inget annat ordinerats. Vid komplett förvirring bör aktivitetsnivån trappas ned under 10 minuter och hållas låg tills det att nya infall och idéer uppstår.</p>
					 <p>Symptom på kongressdeltagande omfattar bland annat: Spontan munterhet, fryntlighet¹, gott humör, positiv anda, kronisk optimism, glädje och entusiasm. Vid akuta symptom, ring och bjud in en god vän till kongressen.</p>
					 <p>Programmet är som vanligt preliminärt tills det att kongressen är över, men hoppas kunna ge en fingervisning om innehållet.</p>
					 </span>

         </div>
       </div>
			 <div class="programm">
			 <table>
				 <tbody>
				 <tr>
					 <th>Tid</th>
					 <th>Aktivitet</th>
						<th>Smittorisk </th>
				 </tr>
				 <tr>
					 <td>15:00</td>
					 <td>Öppning och välkomsttal, kanske en välkomstdrink</td>
					 <td>100%</td>
				 </tr>
				 <tr>
					 <td>15:30</td>
					 <td>Bok- och skyddstips</td>
					 <td>271%</td>
				 </tr>
				 <tr>
					 <td>16:00</td>
					 <td>Debatt: Hur hade Världarnas Krig slutat om martianerna blev infekterade med zombievirus?</td>
					 <td>314%</td>
				 </tr>
				 <tr>
					 <td>17:00</td>
					 <td>Presentation av Martin: Någonting med tentakler</td>
					 <td>360˚</td>
				 </tr>
				 <tr>
					 <td>18:00</td>
					 <td>Paus för intag av mat och dryck</td>
					 <td>451˚F</td>
				 </tr>
				 <tr>
					 <td>19:30</td>
					 <td>Presentation av Patrik C: Väsen från nordisk folktro i svenskspråkig fantasy</td>
					 <td>666%</td>
				 </tr>
				 <tr>
					 <td>20:30</td>
					 <td>Filmvisning och omröstning</td>
					 <td>720%</td>
				 </tr>
				 <tr>
					 <td>21:30</td>
					 <td>Quiz med smittsamma musikstycken</td>
					 <td>100i%</td>
					 <tr>
						 <td>22:18</td>
						 <td>Avtorkning och packetering av medlemmarna</td>
						 <td>Nan</td>
					 </tr>
				 </tr>
			 </tbody>
			 </table>
			 </div>
			 <span class="footnote">
			 	¹: Här återkom begreppet fryntlig från <a href="index.php" target="_blank">startsidan</a>.
			 </span>
     </div>
   </div>

   <script
     src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
     integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
     crossorigin="anonymous"
   ></script>

<?php require_once('php/end.php'); ?>
