<?php

session_start(['read_and_close' => true]);
$auth=(int)$_SESSION['HOSTCONAUTH']??0;

$auth=true;

require_once('php/SMD.php');

class Impl {
	private $db ;

	// Create a database connection handle
	function __construct() {
		$this->db=new PDO("sqlite:data/hostcondb.sqlite");
	}

	// A small helper function to do simple database queries
	private function SimpleSQL(string $query,array $keys,bool $list=false) : mixed {
		$q=$this->db->prepare($query);
		$qok=$q->execute($keys);
		if($qok) {
			if($list) {
				$res=$q->fetchAll(PDO::FETCH_OBJ)?:[];
			} else {
				$res=$q->fetch(PDO::FETCH_OBJ)?:(object)[];
			}
		} else {
			if($list) {
				$res=[];
			} else {
				$res=(object)[];
			}
		}
		return $res;
	}

	// Fetch all info about a single member
	public function GetMember($id) : object {
		return $this->SimpleSQL("select * from Members where id=:id",[':id' => $id]);
	}

	// Fetch a list of members. If authorized, all members - otherwise all visible members.
	public function GetMemberList() : array {
		$res=$this->SimpleSQL("select name,email from Members where visible=true order by name asc",[],true);
		return $res;
	}

	// Fetch a list of all members. 
	public function GetFullMemberList() : array {
		global $auth;
		if($auth) {
			$res=$this->SimpleSQL("select id,name,email from Members order by name asc",[],true);
		} else {
			$res=[];
		}
		return $res;
	}

	// Add new member
	public function AddMember(string $email,string $name,bool $show,string $notes) : void {
		if($email==null || trim($email)=="") {
			throw new Error("E-post saknas");
		}
		if($name==null || trim($name)=="") {
			throw new Error("Namn saknas");
		}
		$tmp=$this->GetMember($email);
		if(isset($tmp->name) && $tmp->name==$name) {
			throw new Error("Redan registrerad");
		}
		$this->SimpleSQL("insert into Members (email,name,visible,notes) values (:email,:name,:show,:notes)",[
			':email' => trim($email),
			':name' => trim($name),
			':show' => $show,
			':notes' => $notes
		]);
	}

	// Delete a member
	public function DelMember(string $id) : void {
		global $auth;
		if($auth) {
			$q=$this->db->prepare("delete from members where id=:id");
			$q->execute([':id' => $id]);
		}
	}

};

$smd=new SMD(new Impl());
$smd->Handle();

?>
