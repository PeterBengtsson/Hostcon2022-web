# Content


## Landing page

När: 12e november 2022 CE
Var: Argostugan
Hur: Fastställs i efterhand

Club Cosmos välkomnar alla till den post-pandemiska höstkongressen (eller som det blir utan prickar över ö – hostkongressen).
Höstens gemytliga lilla SF-kongress i Göteborg – här dryftas Science Fiction i olika former, färger och smaker men även annan
fantastik får plats. Fryntliga fans välkomnas såklart, men vill man komma och beklaga sig över att framtiden var bättre förr
så går även det bra. På kongressen finns det fantastik, fika och filk¹ för alla. 

Bland de nya traditioner vi instiftat genom åren återfinns på kongressen moderna klassiker som exempelvis föredrag, boktips,
filmvisning, gratis medlemskap i kongressen för medlemmar i Club Cosmos och andra trevliga saker.


¹: Ord som [filk](https://en.wiktionary.org/wiki/filk) och [fryntlig](https://www.saob.se/artikel/?unik=F_1539-0143.PM6n)
används för effekt, inte för att göra det svårt. För övrigt ges ingen garanti för förekomsten av någon som helst filk på
kongressen även om det har förekommit [tidigare år](https://clubcosmos.net/video.php?id=2).

## Program


Som kongresser bör, så har även denna ett program med mer eller mindre riskfyllda aktiviteter. Rekommenderad dos aktiviteterär
3-4 per timme om inget annat ordinerats. Vid komplett förvirring bör aktivitetsnivån trappas ned under 10 minuter och hållas
låg tills det att nya infall och idéer uppstår.

Symptom på kongressdeltagande omfattar bland annat: Spontan munterhet, fryntlighet¹, gott humör, positiv anda, kronisk optimism,
glädje och entusiasm. Vid akuta symptom, ring och bjud in en god vän till kongressen.

Programmet är som vanligt preliminärt tills det att kongressen är över, men hoppas kunna ge en fingervisning om innehållet.

|Tid|Aktivitet|Smittorisk|
|---|---------|----------|
|15:00|Öppning och välkomsttal, kanske en välkomstdrink|100%|
|15:30|Glenn förevisar bok- och skyddstips|271%|
|16:00|Debatt: Hur hade Världarnas Krig slutat om martianerna blev infekterade med zombievirus?|314%|
|17:00|Presentation av Martin: Någonting med tentakler|360°|
|18:00|Paus för intag av mat och dryck|451°F|
|19:30|Presentation av Patrik C: Någonting utan tentakler? Med öl?|666%|
|20:30|Filmvisning och omröstning|720%|
|21:30|Andra aktiviteter|100i%|
|22:18|Avtorkning och packetering av medlemmarna|NaN|

¹: Här återkom begreppet fryntlig från [startsidan](index.php).

## Membership

Anmälan till kongressen sker genom att anmälningsformuläret på sidan fylls i - om man inte är medlem i Club Cosmos tas en smärre
avgift om 100kr ut, vilken kan betalas på plats eller i förväg till Club Cosmos plusgiro: 436 77 95 - 4. Fika ingår i medlemsavgiften.

Registrering av deltagande i kongressen kan ske fram till och med den 7e november 2022 CE.

## Location

Club Cosmos ho:stkongresser är tillbaka i Argostugan vilken, senast vi undersökte saken, fortfarande befinner sig på
[Ekedalsgatan 26](http://maps.google.se/maps?f=q&source=s_q&hl=en&geocode=&q=Ekedalsgatan+26,+G%C3%B6teborg&aq=0&sll=61.606396,21.225586&sspn=32.973326,64.951172&ie=UTF8&hq=&hnear=Ekedalsgatan+26,+414+68+G%C3%B6teborg,+V%C3%A4stra+G%C3%B6talands+L%C3%A4n&ll=57.687084,11.931292&spn=0.004399,0.007929&t=h&z=17).
Stugan är inspekterad av Antizombex och var 2022-09-16 helt fri från zombievirus, marsianska virus, vampyrvirus, andromedavirus och miniatyrubåtar.

Vet man inte redan hur man tar sig dit så är närmsta hållplats Ekedal där spårvagn 11 vanligtvis stannar. Sedan är det bara
att bege sig upp för backen och hålla utkik på höger sida.

![Argostugan](images/argostugan.jpg)

## Information

Utan dessa tappra varelsers insatser hade kongressen kanske inte blivit av!

Anna Kashubina 
: Websida - design och kod, ansvarig för zombies

Peter Bengtsson
: Diverse teknik, allmän hjälpreda, ordnar fika, skyldig till tipspromenaden

Louise Rylander
: Kongressgeneral, ansvarig för kongressens framtida historia

Helena Kiel
: Ekonomiskt ansvarig, hanterar berggrunden och ser till att kongressen inte drabbas av jordbävningar eller vulkanutbrott

Lars-Göran Johansson
: Biografanläggning, uppmuntran av medlemmarna, underhåll av cybernetiska livsformer

Glenn Petersen
: Bokrekommendationer, ekrekommendationer

Thomas Olsson
: Baransvarig, barbaransvarig

Rudolf der Hagopian
: Rösträknare, rasträknare, resträknare, testräknare



