<?php

/*
 * SMD Generator for PHP
 * 
 * Generates a Service Mapping Description (SMD) and handles
 * JSON-RPC calls to enable clients to call PHP methods.
 * 
 * Version 1.1
 * 	Requires PHP7 or later
 * 
 * TODO Implement sub-services
 * TODO Implement object descriptions
 * TODO Add support for more transport types.
 * 
 * Changelog:
 *		2018-06-25 (v1.1):
 *			Added "@smd returns=json" for already json-encoded (string) return values.
 *
 * ©2017 Peter Bengtsson <tesla@och.nu>, licensed under the GNU LGPL v3.0 or later
 */

class SMD {
	/*
	 * Global SMD settings.
	 */

	/* This points at the directory in which temporary files are stored. Change as needed. */
	const TMPDIR = '/tmp/';
	/* Local cache timeout is in seconds. 3600s=1h, set to 0 to disable SMD caching. */
	const	CACHETIME = 3600;
	/* Browser/client cache timeout in seconds. */
	const MAXAGE = 300;

	/*============ Private data below ===============*/

	/* Holds a reference to the object implementing the RPC functions. This is examined by reflection to construct the SMD  */
	private $impl=null;

	/* Holds the reflection object for $impl */
	private $refl=null;

	/* SMD descripton as string and object  */
	private $SMD_string=null;
	private $SMD_obj=null;


	/* Map to convert PHP types to SMD types. {{{ */
	private $typemap=array(
		'int' => 'integer',
		'float' => 'number',
		'double' => 'number',
		'string' => 'string',
		'bool' => 'boolean',
		'array' => 'array',
		'object' => 'object',
/*		'resource' => 'object',*/ /* Resources are not handled. */
		'void' => null
	);/*}}}*/

	/*
	 * Cache SMD text so we don't have to regenerate it for every request.
	 */
	private function CacheSMD(string $target) {//{{{
		$fp=(self::CACHETIME>0)?@fopen(self::TMPDIR.'smd-lock','w+'):false;
		if($fp!==false) {
//			$fname=self::TMPDIR."smd-".md5(Reflection::export($this->refl,true));	/* Attempt to create a unique identifier. This does not _quite_ work if the change is minor enough. */
			$fname=self::TMPDIR."smd-".md5((string)$this->refl);	/* Attempt to create a unique identifier. This does not _quite_ work if the change is minor enough. */
			$mtime=@filemtime($fname);
			if($mtime!==false && (time()-$mtime)<self::CACHETIME) {
				flock($fp,LOCK_SH);
				$this->SMD_string=file_get_contents($fname);
			} else {
				$this->SMD_obj=$this->BuildSMD($target);
				flock($fp,LOCK_EX);
				$this->SMD_string=json_encode($this->SMD_obj);
				file_put_contents($fname,$this->SMD_string);
			}
			flock($fp,LOCK_UN);
			fclose($fp);
		} else {	/* Cache disabled, RO filesystem or other issue. Just re-create the SMD every time. */
			$this->SMD_obj=$this->BuildSMD($target);
		}
	}//}}}

	/**
	 * Since the 'implobj' object is inspected by reflection and can be any class this is a strict
	 * as the types can be.
	 *
	 */
	function __construct($implobj,string $endpoint=null,string $smdtext=null) {//{{{
		global $_SERVER;
		if(!is_object($implobj)) {
			throw new Exception("Invalid type for parameter 'implobj', not an object.");
		}
		$this->impl=$implobj;
		$this->refl = new ReflectionObject($this->impl);
		if($smdtext!==null) {
			$this->SMD_string=$smdtext;
		} else {
			$target=($endpoint===null)?basename($_SERVER['REQUEST_URI']):$endpoint;
			$this->CacheSMD($target);
		}
	}//}}}

	/*
	 * Extract properties from methods.
	 * This is used to construct the SMD definition.
	 *
	 */
	private function GetMProps(ReflectionMethod $m) : stdClass {//{{{
		$props=(object)[
			'parameters' => array(),
			'additionalParameters' => false,
			'returns' => (object)['type' => null ],
			'rawresponse' => false
		];
		if($m->hasReturnType()) {
			$rtype=(string)$m->getReturnType();
			if(isset($this->typemap[$rtype])) {
				$props->returns->type=$this->typemap[(string)$m->getReturnType()];
			} else {
				try {
					$ref=new ReflectionClass($rtype);	/* Also works for interfaces */
					$props->returns->type='object';
				} catch(Exception $ex) {
					$props->returns->type=null;
				}
			}
		}
		foreach($m->getParameters() as $p) {
			$po=['name' => $p->getName()];
			if($p->hasType()) {
				$po['type']=$this->typemap[(string)$p->getType()];
			} else if($p->isDefaultValueAvailable()) {	/* Only map default values which are not true, false or null since these are often used as 'special' values. */
				$v=$p->getDefaultValue();
				switch(gettype($v)) {
				case 'string':
				case 'int':
				case 'double':
				case 'float':
				case 'array':
				case 'object':
					$po['type']=$this->typemap[gettype($p->getDefaultValue())];
				break;
				default:// Don't set any type if we can't figure it out.
					;
				}
			}
			if($p->isVariadic()) {
				$props->additionalParameters=true;
			}
			$props->parameters[]=(object)$po;
		}
		/* Doc comments may contain @smd modifiers to further specify how a method behaves */
		$dc=$m->getDocComment();
		if($dc!==false) {
			/* Iterate over all lines containing '@smd ' - The map(filter(explode())) expression returns an array of all lines containing '@smd ', trim()ed. */
			foreach(array_map(function($l){return(trim($l," \t*/"));},array_filter(explode("\n",$dc),function($l){return(strpos($l,'@smd ')!==false);})) as $modline) {
				foreach(preg_split('/[ \t]/',substr($modline,5)) as $mod) {	/* Iterate over the options */
					list($k,$v)=array_merge(explode("=",$mod),[true]);
					switch($k) {
					case 'media':
						$props->media=(object)array_merge(['type' => $v],(array)($props->media??[]));
					break;
					case 'binaryEncoding':
					case 'encoding':
						$props->media=(object)array_merge(['binaryEncoding' => $v],(array)($props->media??[]));
					break;
					case 'returns':
						switch($v) {
						case 'json':
							$props->rawresponse=true;
						break;
						default:
							throw new Exception("Unsupported returns modifier: {$v} in @smd-line of method ".$m->getName().".");
						}
					break;
					default:
						throw new Exception("Unsupported property '{$k}' in @smd-line of method ".$m->getName().".");
					}
				}
			}
		}
		return($props);
	}//}}}

	/*
	 * Returns a string compromising the services
	 * part of the SMD.
	 */
	private function GetServices() : stdClass {//{{{
		$srv=new stdClass;
		foreach($this->refl->getMethods(ReflectionMethod::IS_PUBLIC) as $m) {
			$name=$m->getName();
			if($name[0]!='_') {	// Skip any methods starting with an underscore - we don't want to include constructors etc.
				$srv->{$m->getName()}=$this->GetMProps($m);
			}
		}
		return($srv);
	}//}}}

	/**
	 * Returns the SMD as JSON.
	 */
	public function GetSMD() : string {//{{{
		if($this->SMD_string===null && $this->SMD_obj!==null) {
			$this->SMD_string=json_encode($this->SMD_obj);
		}
		return($this->SMD_string);
	}//}}}

	private function GetSMDObject() : stdClass {//{{{
		if($this->SMD_obj===null && $this->SMD_string!==null) {
			$this->SMD_obj=json_decode($this->SMD_string);
		}
		return($this->SMD_obj);
	}//}}}

	private function BuildSMD(string $target) : stdClass {//{{{
		$smd=(object)[
			'SMDVersion' => '2.0',
			'transport' => 'POST',
			'envelope' => 'JSON-RPC-2.0',
			'contentType' => 'application/json',
			'target' => $target,
			'services' => $this->GetServices()
		];

		return($smd);
	}//}}}

	/**
	 * Call a method on the impl object using positional parameters.
	 */
	function CallByPosition(stdClass $srv,string $method,array $params) {//{{{
		/* Fill in missing parameters with default values */
		foreach($srv->parameters as $idx => $p) {
			if(!isset($params[$idx]) && isset($p->default)) {
				$params[$idx]=$p->default;
			}
		}
		$count_c=count($params);
		$count_m=count($srv->parameters);
		if(($count_c<$count_m) || ($count_c>$count_m && $srv->additionalParameters===false)) {
			throw new Exception("Invalid number of parameters in call to {$method}.",-32602);
		}
		$res=call_user_func_array(array($this->impl,$method),$params);	/* Call the method on the implementation object */
		return($res);
	}//}}}

	/**
	 * Call a method on the impl object using named parameters.
	 */
	private function CallByName(stdClass $srv,string $method,stdClass $params) {//{{{
		$pinfo=array();
		foreach($srv->parameters as $pdef) {
			$pinfo[$pdef->name]=$pdef;
		}
		$ref=new ReflectionMethod($this->impl,$method);
		$callparams=array();
		foreach($ref->getParameters() as $idx => $p) {
			if(isset($params->{$p->getName()})) {
				$callparams[$idx]=$params->{$p->getName()};
			} else if(isset($pinfo[$p->getName()]->default)) {
				$callparams[$idx]=$pinfo[$p->getName()]->default;
			} else {
				throw new Exception("Missing parameter \"{$p->getName()}\" in method call.",-32602);
			}
		}
		$count_c=count($callparams);
		$count_m=count($srv->parameters);
		if(($count_c<$count_m) || ($count_c>$count_m && $srv->additionalParameters===false)) {
			throw new Exception("Invalid number of parameters in call to {$method}.",-32602);
		}
		$res=call_user_func_array(array($this->impl,$method),$callparams);	/* Call the method on the implementation object */
		return($res);
	}//}}}

	private function RPCErr(string $msg,int $code,string $id=null): string {//{{{
		return(json_encode((object)['jsonrpc'=>'2.0','id'=>$id,'error'=>(object)['code'=>$code,'message'=>$msg]]));
	}//}}}

	/**
	 * Turn a JSON-RPC object into a method invocation and return the result.
	 */
	private function JSONCall(stdClass $data) : string {//{{{
		$res=false;
		if($data) {
			try {
				if($this->SMD_obj==null) {
					$this->SMD_obj=@json_decode($this->SMD_string);
					if($this->SMD_obj==null) {
						throw new Exception("Internal error in RPC layer. Unable to retrieve service description.",-32603);
					}
				}
				if(!isset($this->SMD_obj->services->{$data->method}) || !method_exists($this->impl,$data->method)) {
					throw new Exception("Call to unimplemented method \"$method\"",-32601);
				}
				$srv=$this->SMD_obj->services->{$data->method};
				if(is_object($data->params)) {
					$res=$this->CallByName($srv,$data->method,$data->params);
				} else {
					$res=$this->CallByPosition($srv,$data->method,$data->params??[]);
				}
				if(isset($data->id)) {
					if($srv->rawresponse) {
						$res='{"jsonrpc":"2.0","id":"'.$data->id.'","result":'.$res.'}';
					} else {
						$res='{"jsonrpc":"2.0","id":"'.$data->id.'","result":'.json_encode($res).'}';
					}
				} else {
					$res='';
				}
			} catch(Throwable $e) {
				error_log($e->getFile().":".$e->getLine()." ".$e->getMessage());
				$res=$this->RPCErr($e->getMessage(),(int)$e->getCode(),isset($data->id)?$data->id:null);
			}
		}
		return($res);
	}//}}}

	/**
	 * Handle()
	 * Handles a GET/POST request and either returns the SMD or performs one or more
	 * calls to the implementation (returning the results).
	 */
	public function Handle() {//{{{
		global $_SERVER;
		$responsecount=0;
		if($_SERVER['REQUEST_METHOD']=='GET') {
			header("Cache-Control: max-age=".self::MAXAGE);
			header("Content-Type: application/json");
			header("Access-Control-Allow-Origin: *");
			header("Content-Length: ".strlen($this->GetSMD()));
			echo $this->GetSMD();
/* Too restrictive - correct in principle, but breaks some clients.
		} else if($_SERVER['REQUEST_METHOD']=='POST' && isset($_SERVER['CONTENT_TYPE']) && strncmp($_SERVER['CONTENT_TYPE'],'application/json',16)) {
			header("HTTP/1.1 415 Unsupported content-type for request. Only application/json supported.");
*/
		} else if($_SERVER['REQUEST_METHOD']=='POST') {
			$call=@json_decode(file_get_contents("php://input"));
			if($call===false) {
				$res=$this->RPCErr('Error parsing request',-32700);
				$responsecount=1;
			} else if(is_array($call)) {
				$res=array();
				foreach($call as $c) {
					if(isset($c->method) && isset($c->jsonrpc) && $c->jsonrpc=='2.0') {
						$x=$this->JSONCall($c);
						if($x!==false && isset($c->id)) {
							$res[$responsecount++]=$x;
						}
					} else if(isset($c->jsonrpc) && $c->jsonrpc!='2.0') {
						$res[$responsecount++]=$this->RPCErr('Unsupported RPC version, this server only understands JSON-RPC v2.0',-32000,(isset($c->id))?$c->id:-1);
					} else {
						$res=$this->RPCErr('Not a valid json-rpc request object',-32600);
					}
				}
				if(gettype($res)=='array') {
					$res="[".implode(',',$res)."]";
				}
			} else if(!isset($call->method) || !isset($call->jsonrpc)) {
				$res=$this->RPCErr('Not a valid json-rpc request object',-32600);
				$responsecount=1;
			} else if($call->jsonrpc!='2.0') {
				$res=$this->RPCErr('Unsupported RPC version, this server only understands JSON-RPC v2.0',-32000,(isset($call->id)?$call->id:null));
				$responsecount=1;
			} else {
				$res=$this->JSONCall($call);
				if(isset($call->id)) {
					$responsecount++;
				}
			}

			if($responsecount>0) {	/* If responsecount is zero, we only got notifications and there is no answer to send back */
				if($res===false) {
					$res=$this->RPCErr('RPC server error',-32603);
				}
				header("Cache-Control: no-cache, no-store");
				header("Access-Control-Allow-Origin: *");
				header("Content-Type: application/json");
				header("Content-Length: ".strlen($res));
				echo $res;
			} else {
				header("HTTP/1.1 204 No response content");
				header("Access-Control-Allow-Origin: *");
				header("Cache-Control: no-cache, no-store");
				header("Content-Type: application/json");
				header("Content-Length: 0");
			}
		} else if($_SERVER['REQUEST_METHOD']=='HEAD') {
			header("Content-Type: application/json");
			header("Access-Control-Allow-Origin: *");
			header("Content-Length: ".strlen($this->SMD_string));
		} else {
			header("HTTP/1.1 501 Unimplemented method.");
		}
	}//}}}

}
/* vi: set ts=3 foldmethod=marker: */
?>
