
pragma encoding='utf-8';

create table Members (
	id	integer,	-- SQLite automatically creates a key if not supplied
	email	text,
	name	text,
	notes	text,
	visible	boolean default true,
	primary key(id)
);

