<?php require_once('php/head.php'); ?>

<?php require_once('php/start.php'); ?>

         <div class="col-md-6">
           <span class="information-index">
             <p>
            När: 12e november 2022 CE kl. 15:00 CET
            <br> Var: Argostugan
            <br> Hur: Fastställs i efterhand
           </p>
           <p>
             Club Cosmos välkomnar alla till den post-pandemiska höstkongressen (eller som det blir utan prickar över ö – hostkongressen). Höstens gemytliga lilla SF-kongress i Göteborg – här dryftas Science Fiction i olika former, färger och smaker men även annan fantastik får plats. Fryntliga fans välkomnas såklart, men vill man komma och beklaga sig över att framtiden var bättre förr så går även det bra. På kongressen finns det fantastik, fika och filk¹ för alla.
           </p>
           <p>
           Bland de nya traditioner vi instiftat genom åren återfinns på kongressen moderna klassiker som exempelvis föredrag, boktips, filmvisning, gratis medlemskap i kongressen för medlemmar i Club Cosmos och andra trevliga saker.
         </p>
         </span>
         </div>

         <span class="footnote">
           ¹: Ord som <a href="https://en.wiktionary.org/wiki/filk" target="_blank">filk</a> och <a href="https://www.saob.se/artikel/?unik=F_1539-0143.PM6n" target="_blank">fryntlig</a> används för effekt, inte för att göra det svårt. För övrigt ges ingen garanti för förekomsten av någon som helst filk på kongressen även om det har förekommit <a href="https://clubcosmos.net/video.php?id=2" target="_blank">tidigare år</a>.
         </span>

   <script
     src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
     integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
     crossorigin="anonymous"
   ></script>

<?php require_once('php/end.php'); ?>
