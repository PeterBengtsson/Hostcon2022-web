<?php
session_start();
if(isset($_GET['logout'])) {
	$_SESSION['HOSTCONAUTH']='';
	header("Location: index.php");
	exit(0);
}

if(!isset($_SESSION['HOSTCONAUTH']) || $_SESSION['HOSTCONAUTH']!=1) {
	header("Location: login.php");
	exit(0);
}
require_once('php/head.php');
?>
<style type=text/css>
#members {
	background-color: #808080;
}
#members option {
	background-color: #ffffaa;
	color: #000000;
}
#infotable {
	margin-right: auto;
	margin-left: 0;
	width: 100%;
}
#infotable tr th:first-child {
	width: 6ch;
}
</style>
<script type="text/javascript">
//for admin panel

var rpc;
const q=(s) => document.querySelector(s);

async function show_member() {
	let key = document.querySelector("#members").value;
	const info = await rpc.GetMember(key);

	document.querySelector("#data_id").textContent=info.id;
	document.querySelector("#data-name").textContent=info.name;

	document.querySelector("#data-email").textContent=info.email;

	document.querySelector("#data-visible").textContent=(info.visible)?"Yes":"No";

	document.querySelector("#data-notes").textContent=info.notes;
}

async function make_memberlist() {
	const members=await rpc.GetFullMemberList();
	const t=q("#memberlist");
	const sel=q("#members");

	members.forEach( (info) => {
		let row=t.insertRow(-1);
		row.insertCell(-1).textContent=info.id;
		row.insertCell(-1).textContent=info.name;
		row.insertCell(-1).textContent=info.email;

		let opt=document.createElement("OPTION");
		opt.value=info.id;
		opt.textContent=`${info.name} <${info.email}>`;
		sel.append(opt);
	})

	q("#count").textContent=members.length;

};

async function add_member() {
	const name=q("#newname").value;
	const email=q("#newemail").value;
	const note=q("#newnote").value;
	const visible=q("#newvisible").checked;
	await rpc.AddMember(email,name,visible,note);
	document.location.reload();
}

async function del_member() {
	let key=document.querySelector("#members").value;
	if(window.confirm(`You really wanna delete member ${key}?`)) {
		await rpc.DelMember(key);
		document.location.reload();
	}
}

async function init() {
	const desc = await fetch("userapi.php");
	rpc=new SMDClient(await desc.text(),{async:true});

	make_memberlist();

	q("#members").addEventListener("change",show_member,false);

	document.querySelector("#delmember").addEventListener("click",del_member,false);
}

window.addEventListener("load", init, {once:true});
</script>
<?php require_once('php/start.php'); ?>

<div class="col-md-6">
<h2>Enter e-mail</h2>

<select id=members size=6></select><br>

<button id=delmember>Delete member</button>

<table id=infotable>
<tr><th>Id<td id=data_id>
<tr><th>Name<td id=data-name>
<tr><th>E-Mail<td id=data-email>
<tr><th>Visible<td id=data-visible>
<tr><th>Notes<td id=data-notes>
</table>
</div>

<h3>Number of members: <span id=count></span></h3>

<table id="memberlist">
	<tr><th>Id<th>Name<th>E-mail
</table>

<a href="?logout">Logout</a>

<?php require_once('php/end.php'); ?>
